package usuarios;

import java.io.BufferedReader;
import java.io.EOFException;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.RandomAccessFile;

import usuarios.administracion.LeerUsuarios;

public class UsuarioRaso extends Usuario{
	
	/**
	 * Le paso el par�metro del usuario que voy a modificar
	 * @param usuario
	 */
	@Override
	public void modificarUsuario(Usuario unUsuario){
		try{
			RandomAccessFile f = new RandomAccessFile("usuarios.txt", "rw");
			BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
			//Leer el fichero 
			boolean fin=false;
			boolean encontrado=false;
			String linea;
			f.seek(0);
			
			do{
				try{
					linea= f.readUTF(); //Si encuentro el nombre, modifico el nombre y la pass
                    long puntero = f.getFilePointer();
                    if(linea.trim().equalsIgnoreCase(unUsuario.getNick())){
                    	
                        File file = new File("notas"+unUsuario.getNick()+".txt");
                        puntero-=17;
                        f.seek(puntero);
                        System.out.println("Se ha encontrado el nick.");
                        encontrado=true;
                        
                        System.out.println("Nuevo nick:");
                        String nick = in.readLine();
                        System.out.println(puntero);
                        f.writeUTF(LeerUsuarios.formatearString(nick, 15));
                        
                        System.out.println("Nueva contrase�a:");
                        f.writeUTF(LeerUsuarios.formatearString(in.readLine(), 15));
                        
                        File file2 = new File ("notas"+ nick.trim() + ".txt");
                        file.renameTo(file2);

                        linea = f.readUTF();
					}else{
						f.readUTF();
						f.readInt();
					}
					
				}catch(EOFException e){
					System.out.println("Fin del fichero.");
					if(!encontrado){
						System.out.println("Usuario no encontrado.");
					}
					f.close();
					LeerUsuarios.pasarRafArrayList();
					fin = true;
				}
			}while(!fin);
		}catch(IOException e){
			System.out.println("Error, "+e.getMessage());
		}
	
	}
	
	/**
	 * M�todo ToString
	 * @return String
	 */
	@Override
	public String toString() {
		return "Tipo usuario: UsuarioRaso \nNick:" + nick + "\nPassword:" + password + 
				"\nNombre fichero: " + notasUsuario.getNombreFichero() +"\nEdad: "+ edad;
	}

	
	
}
