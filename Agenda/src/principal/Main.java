package principal;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import menu.agenda.Menu;
import usuarios.Usuario;
import usuarios.administracion.LeerUsuarios;
import usuarios.objetos.Logear;
import usuarios.objetos.Registrar;

public class Main {

	
	public static void main(String[] args){
		new Main().init();
	}
	
	/**
	 * M�todo que muestra el inicio del programa.
	 */
	public void init(){
		BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
		LeerUsuarios.pasarRafArrayList();
		try {
			int opcion = -1;
			while(opcion!=0 && (opcion<1 || opcion>2)) {
				System.out.println("Bienvenido.\nQu� desea hacer:\n[1] loguear\n[2] Registrarse\n[0] Salir");
				opcion = Integer.parseInt(in.readLine());
				switch(opcion){
				case 0:
					System.exit(0);
					break;
				case 1:
					Usuario tipoUsuario = Logear.logearUsuario();
					Menu.visualizarMenu(tipoUsuario);
					break;
				case 2:
					Registrar.registrarUsuario();
					break;
				}
			}
		} catch (NumberFormatException | IOException e) {
			System.out.println("Error "+e.getMessage());
		}
	}
	
	
}
