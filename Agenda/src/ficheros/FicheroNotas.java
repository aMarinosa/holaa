package ficheros;

import java.io.BufferedReader;
import java.io.EOFException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.RandomAccessFile;
import java.time.LocalDate;

import usuarios.Usuario;

public class FicheroNotas {

	private String nombreFichero;
	private RandomAccessFile raf;
	private BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
	
	/**
	 * Nombre del fichero (Constructor)
	 * @param usuario
	 */
	public FicheroNotas(Usuario usuario) {
		nombreFichero = usuario.getNotasUsuario().getNombreFichero();
	}
	
	/**
	 * M�todo que me crea el fichero y se utiliza para escribir en �l.
	 */
	public void crearFichero() { //RAF
		RandomAccessFile raf;
		try {
			
			raf = new RandomAccessFile(nombreFichero, "rw");
			//Escribir
			String linea = null;
			boolean fin = false;
			String lineas="";
			try {
				System.out.println("Para salir, escriba /fin");
				raf.seek(raf.length());
				do {
					linea = in.readLine();
					if(linea.equalsIgnoreCase("/fin")) {
						fin = true;
						break;
					}
					lineas+=linea+"\n";
				}while(!fin);
			}catch (Exception e) {
				System.out.println("Error, "+e.getMessage());
			}
			
			//busco la posici�n donde est� la fecha
			long posicionpunterofecha =buscarFecha(LocalDate.now());
			// si la posicion est� al final del fichero o al principio del fichero (pos=0) then
			if(posicionpunterofecha==raf.length() || raf.length()==0){
				//escribo en el fichero la fecha.
				raf.writeUTF("Fecha "+LocalDate.now()+":\n"+lineas);
			}
			//si no
			else{
				//me posisicono en la posicion del fichero despu�s de la fecha
				raf.seek(posicionpunterofecha);
				//leo el siguiente
				String temporal=raf.readUTF();
				//me vuelvo a posicionar en la posicion del puntero
				raf.seek(posicionpunterofecha);
				//escribo 
				raf.writeUTF(temporal+"\n"+lineas);
			}
			raf.close();
		} catch (IOException e) {
			System.out.println("Error, "+e.getMessage());
		}
		
	}

	/**
	 * Buscamos la posicion de la fecha para comprobar que no se repita si escribimos varias veces al d�a
	 * @param date
	 * @return posici�n de la fecha
	 */
	public long buscarFecha(LocalDate date){ 
		
		try {
			raf = new RandomAccessFile(nombreFichero, "r");
			long ultimaposicion=0;
			while(raf.getFilePointer()<raf.length()){
				ultimaposicion=raf.getFilePointer();
				String linea = raf.readUTF();
				String fecha = linea.substring(6, 16);
				if(date.equals(LocalDate.parse(fecha))){
					return ultimaposicion;
				}
			}
			return raf.length();
			
		} catch (IOException e) {
			System.out.println(e.getMessage());
		}
		return 0;
	}
	
	/**
	 * Visualizamos el archivo
	 */
	public void visualizarFichero() {
		try {
			raf = new RandomAccessFile(nombreFichero, "r");
			boolean fin = false;
			raf.seek(0);
			do {
				try {
					String linea = raf.readUTF();
					System.out.println(linea);
				}catch(EOFException e) {
					System.out.println("Fin de fichero");
					raf.close();
					fin = true;					
				}
			}while(!fin);
		} catch (IOException e) {
			System.out.println(e.getMessage());
		}
	
	}
}
