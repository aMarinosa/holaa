package usuarios.objetos;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

import principal.Main;
import usuarios.Usuario;
import usuarios.UsuarioAdmin;
import usuarios.UsuarioRaso;
import usuarios.administracion.AdministrarUsuarios;
import usuarios.administracion.LeerUsuarios;

public class Registrar {
	
	/**
	 * M�todo para registrar el usuario
	 */
	public static void registrarUsuario() {
		
		System.out.println("---REGISTRO---");
		
		String nick = pedirNick();
		String password = pedirPass();
		int edad = pedirEdad();
		Usuario usuario =null;
		
		
		//Si el nombre de usuario empieza por 'admin', se crear� como usuario Admin, si no se crear� como usuario Raso
		if(nick.length()>4 && nick.substring(0, 5).equals("admin")) {
			usuario = new UsuarioAdmin();
		}else {
			usuario = new UsuarioRaso();
		}
		
		usuario.setNick(nick);
        usuario.setPassword(password);
        usuario.setEdad(edad);
        
        //AdministrarUsuarios.aniadirUsuario(usuario);
        AdministrarUsuarios.asignarFicheroUsuario(usuario);

        LeerUsuarios.registrarRaf(usuario);
        new Main().init();
	}
	
	
	/**
	 * M�todo que te retorna la edad que ha dado el usuario.
	 * @return
	 */
	private static int pedirEdad() {
		BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
		System.out.println("Edad:");
		int edad = 0;
		try {
			edad = Integer.parseInt(in.readLine());
		} catch (NumberFormatException e) {
			System.out.println("Error, "+e.getMessage());
		} catch (IOException e) {
			System.out.println("Error, "+e.getMessage());
		}
		return edad;
	}
	/**
	 * M�todo que te retorna la contrase�a del usuario despu�s de comprobar que la ha escrito bien.
	 * @return
	 */
	private static String pedirPass() {
		BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
		String password = null;
		do{
			try {
				System.out.println("Escriba su nueva contrase�a (Max 15):");
				password = in.readLine();
				
				System.out.println("Vuelva a escribir su contrase�a para confirmar:");
				String passwordC = in.readLine();
				
				if(passwordC.equals(password)){
					System.out.println("Las contrase�as son iguales! Se crear� la cuenta.");
					return password;
				}else{
					System.out.println("Las contrase�as no coinciden.");
				}
			} catch (IOException e) {
				System.out.println("Error, "+e.getMessage());
			}
			return password;
		}while(true);
	}

	/**
	 * M�todo que te retorna el nick despu�s de comprobar que no existe otro igual.
	 * @return
	 */
	private static String pedirNick() {
		BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
		String nick = null;
		try {
			System.out.println("Ingrese un nombre de usuario (Max 15 caracteres):");
			nick = in.readLine();
			
			while(AdministrarUsuarios.buscarUsuario(nick)) {
				System.out.println("El nombre que ha elegido ya existe. Elija otro.");
				nick = in.readLine();
			}
		
			System.out.println("El nick est� libre.");
			return nick;
			
		} catch (IOException e) {
			System.out.println("Error, "+e.getMessage());
		}
		return nick;
	}
}
