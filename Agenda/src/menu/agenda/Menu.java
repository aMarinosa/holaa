package menu.agenda;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.time.LocalDate;

import agenda.objetos.MostrarCalendario;
import ficheros.FicheroNotas;
import principal.Main;
import usuarios.Usuario;
import usuarios.UsuarioAdmin;
import usuarios.administracion.AdministrarUsuarios;

public class Menu {

	/**
	 * M�todo que me visualiza el men�
	 * @param unUsuario
	 */
	public static void visualizarMenu(Usuario unUsuario){
		BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
		FicheroNotas fich = new FicheroNotas(unUsuario);
		int opcion=-1;
		int opcion2 = -1;
		
		do{
			System.out.println("-----[MENU]-----"
					+ "\n[0]Salir"
					+ "\n[1]Block de notas"
					+ "\n[2]Ver Calendario"
					+ "\n[3]Ver Perfil");
			if(unUsuario instanceof UsuarioAdmin){
				System.out.println("[4]Moficar otros usuarios"
						+ "\n[5]Visualizar todas las cuentas de los usuarios"
						+ "\n[6]Ficheros de los usuarios");
			}
			try {
				opcion = Integer.parseInt(in.readLine());
				while(opcion<0 || opcion>7){
					System.out.println("Opcion no encontrada, elija un valor entre 0 y 7.");
					opcion = Integer.parseInt(in.readLine());
				}
				switch(opcion) {
				case 0:{
					new Main().init();
					break;
				}
				case 1:{
					System.out.println("[0]Ir atras\n[1]Escribir fichero\n[2]Ver fichero");
					opcion2 = Integer.parseInt(in.readLine());
					switch(opcion2) {
					case 0:
						break;
					case 1: 
						fich.crearFichero();
						break;
					case 2:
						fich.visualizarFichero();
						break;
					}
					break;
				}
					
				case 2:{
					MostrarCalendario.anio(LocalDate.now().getYear());
					break;
				}
				case 3:{
					System.out.println("[1] Ver tus datos\n[2] Modificar tu nick / contrase�a \n[3] Salir");
					opcion2 = Integer.parseInt(in.readLine());
					switch(opcion2) {
					case 1:
						System.out.println(unUsuario.toString());
						System.out.println("Tipo: "+AdministrarUsuarios.clasificacionSegunEdad(unUsuario).toString());
						System.out.println("\tCodigo: "+AdministrarUsuarios.clasificacionSegunEdad(unUsuario).getCodigo());
						System.out.println("\tOficio: "+AdministrarUsuarios.clasificacionSegunEdad(unUsuario).getOficio());
						break;
					case 2:
						unUsuario.modificarUsuario(unUsuario);
						break;
					case 3:
						break;
					}
				}
					break;
				}
				
				if(unUsuario instanceof UsuarioAdmin)
				{
					switch(opcion) {
					case 4:{
						UsuarioAdmin.visualizarArchivoUsuarios();
						System.out.println("Nick del usuario al que quieres modificar:");
						String nick = in.readLine();
						if(AdministrarUsuarios.buscarUsuario(nick)) {
							Usuario usuarioModificar = AdministrarUsuarios.cogerUsuarios(nick);
							((UsuarioAdmin) unUsuario).modificarUsuario(usuarioModificar);
						}else {
							System.out.println("No se ha encontrado el usuario.");
						}
						break;
					}
					case 5:{
						System.out.println("[1] Visualizar usuarios desde fichero"
								+ "\n[2] Visualizar usuarios desde vector (Con enumeraciones)"
								+ "\n[3] Visualizar usuarios ordenados"
								+ "\n[4] Visualizar Usuario con m�s edad y con menos edad"
								+ "\n[5] Ir atras");
						opcion2 = Integer.parseInt(in.readLine());
						switch(opcion2) {
						case 1:{
							System.out.println("---USUARIOS SIN ORDENAR---");
							UsuarioAdmin.visualizarArchivoUsuarios();
							break;
						}
						case 2:{
							System.out.println("---USUARIOS SIN ORDENAR---");
							AdministrarUsuarios.visualizarUsuarios();
							break;
						}
						case 3:{
							System.out.println("\n---USUARIOS ORDENADOS---");
							AdministrarUsuarios.visualizarUsuariosOrdernadosPorNombre();
							break;
						}
						case 4:{
							AdministrarUsuarios.visualizarUsuarioMayorYMenor();
							break;
						}
						case 5:{
							break;
						}
						}
						break;
					}
					}	
				}
			} catch (NumberFormatException e) {
				System.out.println("Error, "+e.getMessage());
			} catch (IOException e) {
				System.out.println("Error, "+e.getMessage());
			}
		}while(opcion!=0);
	}
	
}
