package usuarios.administracion;

public enum Edad {

	JOVEN(1, "Estudiante"), 
	ADULTO(2, "Trabajador"), 
	ANCIANO(3, "Jubilado");
	
	private String oficio;
	private int codigo;
	
	private Edad(int codigo, String oficio) {
		this.codigo = codigo;
		this.setOficio(oficio);		
	}

	public String getOficio() {
		return oficio;
	}

	public void setOficio(String oficio) {
		this.oficio = oficio;
	}

	public int getCodigo() {
		return codigo;
	}

	public void setCodigo(int codigo) {
		this.codigo = codigo;
	}
	
	
}
