package usuarios.objetos;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

import principal.Main;
import usuarios.Usuario;
import usuarios.administracion.AdministrarUsuarios;

public class Logear{

	static BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
		/**
		 * M�todo para retornar el usuario logeado
		 * @return
		 */
	public static Usuario logearUsuario() {
		Usuario usuarioLog=null;
		try {
			
			String nick;
			String password;
			boolean fin = true;
			while(fin){
				System.out.println("Introduce tu nombre de usuario:");
				nick = in.readLine();
				
				//Llama al m�todo buscarUsuario para comprobar si existe
				boolean existe = AdministrarUsuarios.buscarUsuario(nick);
				if(existe) {
					
					System.out.println("Se ha encontrado el usuario");
					System.out.println("Introduce la contrase�a:");
					password = in.readLine();
					//Aqu� se comprueba que la contrase�a corresponde al nick
					if(AdministrarUsuarios.comprobarPassUsuario(nick, password)) {
						System.out.println("Se ha encontrado el usuario.");
                        for (Usuario user : AdministrarUsuarios.getvUsuarios()) {
                            if(user.getNick().trim().equalsIgnoreCase(nick)) {
                                return user;
                            }  
                        }
					}else {
						System.out.println("Contrase�a incorrecta.");
						new Main().init();
					}
				}else {
					System.out.println("No se ha encontrado el usuario.");
					new Main().init();
				}		
				
			}
		} catch (IOException e) {
			System.out.println(e.getMessage());
		}
		return usuarioLog;
	}
	

}

