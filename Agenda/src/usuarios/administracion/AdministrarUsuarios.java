package usuarios.administracion;


import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

import usuarios.Usuario;

public class AdministrarUsuarios {

	private static ArrayList<Usuario> vUsuarios= new ArrayList<Usuario>();
	
	/**
	 * Pasa el usuario por par�metro y lo a�ade al Array.
	 * @param usuario
	 */
	public static void aniadirUsuario(Usuario usuario) {
		vUsuarios.add(usuario);
	}
	
	/**
	 * Comprobar si existe el nick 
	 */
	public static boolean buscarUsuario(String nick){
		for(Usuario usuarioR : vUsuarios){
			if(usuarioR.getNick().trim().equalsIgnoreCase(nick)) {
				return true;
			}
		}
		return false;
	}
	
	/**
	 * Comprobar si la password pertenece a un nick en concreto.
	 */
	public static boolean comprobarPassUsuario(String nick, String pass){
		for(Usuario usuarioR : vUsuarios){
			if(usuarioR.getNick().trim().equalsIgnoreCase(nick)) {
				if(usuarioR.getPassword().trim().equalsIgnoreCase(pass)) {
					return true;
				}
			}
		}
		return false;
	}
	/**
	 * Se le pasa el par�metro del usuario nuevo y se le asigna un bloc de notas.
	 * @param usuario
	 */
	public static void asignarFicheroUsuario(Usuario usuario) {
		String nombreFichero = ("notas"+usuario.getNick().trim()+".txt");
		usuario.getNotasUsuario().setNombreFichero(nombreFichero);
		
	}
	/**
	 * Retorna el usuario mediante el par�metro nick
	 * @param nick
	 * @return usuario
	 */
	public static Usuario cogerUsuarios(String nick) {
		for(Usuario unUsuario: vUsuarios) {
			if(unUsuario.getNick().equals(nick)) {
				return unUsuario;
			}
		}
		return null;
	}
	
	/**
	 * Le paso el usuario para que me diga qu� clasificaci�n tienen
	 * @param usuario
	 * @return clasificacion edad
	 */
	public static Edad clasificacionSegunEdad(Usuario usuario) {
		if(usuario.getEdad()<=21){
			return Edad.JOVEN;
		}
		else if(usuario.getEdad()>=22 && usuario.getEdad()<=50){
			return Edad.ADULTO;
		}
		else{
			return Edad.ANCIANO;
		}
	}
	
	/**
	 * Visualizaci�n de todos los usuarios
	 */
	public static void visualizarUsuarios() {
		for(Usuario unUsuario: vUsuarios) {
			System.out.println(unUsuario.toString());
			System.out.println(clasificacionSegunEdad(unUsuario).toString());
			System.out.println("Codigo: "+clasificacionSegunEdad(unUsuario).getCodigo());
			System.out.println("Oficio: "+clasificacionSegunEdad(unUsuario).getOficio());
		}
	}
	
	/**
	 * M�todo el cual visualiza el usuario con m�s edad y el usuario con menos edad
	 */
	public static void visualizarUsuarioMayorYMenor() {
		Usuario usuMax = null;
		int max = 0;
		for(Usuario unUsuario: vUsuarios) {
			if(max<unUsuario.getEdad()) {
				max = unUsuario.getEdad();
				usuMax = unUsuario;
			}
		}
		Usuario usuMin = null;
		int min = max;
		for(Usuario unUsuario: vUsuarios) {
			if(unUsuario.getEdad()<min) {
				min = unUsuario.getEdad();
				usuMin = unUsuario;
			}
		}
		System.out.println("\n---USUARIO M�S MAYOR---");
		System.out.println("El usuario con m�s edad es: "+usuMax.getNick()+" con "+usuMax.getEdad()+" a�os.");
		System.out.println("\n---USUARIO M�S PEQUE�O---");
		System.out.println("El usuario con menos edad es: "+usuMin.getNick()+" con "+usuMin.getEdad()+" a�os.\n");
	}
	
	/**
	 * Visualizar los usuarios ordenados
	 */
	public static void visualizarUsuariosOrdernadosPorNombre() {
		Collections.sort(vUsuarios, new Comparator<Usuario>() {
			public int compare(Usuario usu1, Usuario usu2) {
			      return usu1.getNick().compareTo(usu2.getNick());
			}
		});
		for(Usuario usuario: vUsuarios){
			if(usuario.getNick().length()<6) {
				System.out.println(usuario.getNick()+"\t\t"+usuario.getPassword()+"\t"+usuario.getEdad());
			}else {
				System.out.println(usuario.getNick()+"\t"+usuario.getPassword()+"\t"+usuario.getEdad());
			}
			
		}
	}
	
	public static ArrayList<Usuario> getvUsuarios() {
		return vUsuarios;
	}

	public static void setvUsuarios(ArrayList<Usuario> vUsuarios) {
		AdministrarUsuarios.vUsuarios = vUsuarios;
	}
}
