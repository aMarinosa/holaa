package usuarios.administracion;

import java.io.EOFException;
import java.io.IOException;
import java.io.RandomAccessFile;

import usuarios.Usuario;
import usuarios.UsuarioAdmin;
import usuarios.UsuarioRaso;

public class LeerUsuarios {
	
	/**
	 * M�todo que pasa el archivo raf al arrayList.
	 */
	public static void pasarRafArrayList(){
		boolean fin = false;
		AdministrarUsuarios.getvUsuarios().clear();
		try {
			
			RandomAccessFile f = new RandomAccessFile("usuarios.txt", "rw");
			f.seek(0);
			do{
				Usuario usuario;
				String linea = "";
				try {
					linea = f.readUTF();
					if(linea.trim().length()>4 && linea.substring(0, 5).equals("admin")) {
						
						usuario = new UsuarioAdmin();
					
					}else {
						usuario = new UsuarioRaso();
					}
					
					usuario.setNick(linea.trim());
					usuario.setPassword(f.readUTF().trim());
					usuario.setEdad(f.readInt());
					AdministrarUsuarios.asignarFicheroUsuario(usuario);
					AdministrarUsuarios.getvUsuarios().add(usuario);
				}catch(EOFException e) {
					f.close();
					fin = true;
				}
			}while(!fin);
			
		} catch (IOException e) {
			System.out.println("Error, "+e.getMessage());
		}
	}
	
	/**
	 * Escribirlo en el fichero una vez comprobado
	 */
	public static void registrarRaf(Usuario usuario){
		RandomAccessFile f;
		
		String nickRaf = formatearString(usuario.getNick(), 15);
		String passRaf= formatearString(usuario.getPassword(), 15);
		try {
			f= new RandomAccessFile("usuarios.txt", "rw");
			f.seek(f.length());
			f.writeUTF(nickRaf);
			f.writeUTF(passRaf);
			f.writeInt(usuario.getEdad());
			
			f.close();
		} catch (IOException e) {
			System.out.println(e.getMessage());
		}
		
	}
	
	/**
	 * Poner el mismo tama�o en todos los nick/contrase�as
	 */
	public static String formatearString(String nick, int longitud){
		if(nick.length()<longitud){
			for(int i = nick.length(); i < longitud;i++){
				nick += " ";
			}
			return nick;
		}else if(nick.length()>longitud){
			System.out.println("Se acortar� el nombre, ahora ser�: "+nick.substring(0, longitud));
			return nick.substring(0, longitud);
		}
		return nick;
	}
}
